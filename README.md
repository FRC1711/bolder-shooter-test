# Bolder Motor Test - README #

This repository is for the Bolder Motor Test code that was created Wednesday 2/3/16


### What is this repository for? ###

* Test of Shooter code and how to setup a repository for version control
* To help team members learn about Distributed Version Control Systems
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Have a Bitbucket account, pretty sure most 1711 developers do
* have installed SourceTree on your laptop
* Login to Bitbucket website (which if your reading this, you most likely already have.
* Clone this Repository, which will give you a small window the URL for the Repository, and a button you can press that will clone it automatically into SourceTree.  Make sure you give the destination path to folder in your Eclipse Workspace path
* Open Eclipse and goto file Import.  Browse to find the folder with the Java code and import it.

### Another Section ###