/**
 * Date: Feb 3rd 2016
 * 
 */
package org.usfirst.frc.team1711.robot;

//IP address of machine: 10.17.11.129
import edu.wpi.first.wpilibj.SampleRobot;
import edu.wpi.first.wpilibj.Servo;
import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.CounterBase;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.Joystick.ButtonType;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.VictorSP;
import edu.wpi.first.wpilibj.Talon;


/**
 * This is a demo program showing the use of the RobotDrive class, specifically it 
 * contains the code necessary to operate a robot with tank drive. 
 *
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the SampleRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the manifest file in the resource
 * directory.
 *
 * WARNING: While it may look like a good choice to use for your code if you're inexperienced,
 * don't. Unless you know what you are doing, complex code will be much more difficult under
 * this system. Use IterativeRobot or Command-Based instead if you're new.
 */
public class Robot extends SampleRobot {
//    RobotDrive myRobot;  // class that handles basic drive operations
    Joystick leftStick;  // set to ID 1 in DriverStation
    Talon shooterPitch;
    VictorSP shooterFire;
//    Encoder encoder;
    Servo servo;
    Servo servo1;
    Encoder encoder1; 
    
    public Robot() {
//        myRobot = new RobotDrive(2, 3);
//        myRobot.setExpiration(0.1);
        leftStick = new Joystick(0);
        shooterPitch = new Talon(1);
        shooterFire = new VictorSP(0);
//        encoder = new Encoder(4,0,false,CounterBase.EncodingType.k4X);
        servo = new Servo(7);
        servo1 = new Servo(6);
        encoder1 = new Encoder(4,3,false);
        
        
        //Setting Distance Per Pulse
//        encoder.setDistancePerPulse(0.25); 
        
    }

    
    /**
     * Runs the motors with tank steering.
     */
	public void operatorControl() {
		boolean b = false; 
		boolean a = false; 
		double error1, error2; 
		double Kp = .00005; 
		double error; 
		double currentPWM = 0.5; 
		boolean is90 = false; 
	     
		// myRobot.setSafetyEnabled(true); 
		while (isOperatorControl() && isEnabled()) {
	       // myRobot.tankDrive(leftStick, rightStick); 
			
			double pitchValue = leftStick.getY(); 
			double fireValue = leftStick.getX(); 
			double desiredRpm = (leftStick.getZ()-1); 
			
		shooterFire.set(desiredRpm);
			shooterPitch.set(-pitchValue); 
		//	shooterFire.set(fireValue/4); 
		
			
			/* if (leftStick.getRawButton(6) && (servo.getAngle() < 5.0 && servo.getAngle() > -5.0))
			{
				servo.setAngle(90.0); 
			}
			else if (leftStick.getRawButton(6) && servo.getAngle() > 85.0 && servo.getAngle() < 95.0)
			{
				servo.setAngle(0.0); 
			}
			*/
        
			//System.out.println(leftStick.getZ()); 
			
			int encoder1Value = encoder1.get();
			
			System.out.println(encoder1Value);
			
			// firing trigger and pusher servo
			
			if (!leftStick.getRawButton(1) && is90 == true) 
			{
				servo.setAngle(60.0); 
				is90 = false; 
			}
			else if(leftStick.getRawButton(1) && is90 == false) 
			{
				servo.setAngle(0.0); 
				is90 = true; 
				shooterFire.set(-.6);
			
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				shooterFire.set(0.0); 
			}
			
						
			//int encoderValue = encoder.get();
			
			if (leftStick.getRawButton(9)) 
			{
				//System.out.println(desiredRpm);
			}
			
			
	
					
/*			if(!leftStick.getRawButton(3)){
				shooterFire.set(fireValue);
			}
*/
/*			if (leftStick.getRawButton(3) == true && a == false) 
			{
				a = true; 
			}
				
			else if (leftStick.getRawButton(3) == true && a == true) 
			{
					a = false; 
			}
			
			if (a == true && b == false) {
				shooterFire.set(leftStick.getZ()-1); 
				System.out.println(desiredRpm);
			}
			
				
			
			if (leftStick.getRawButton(2) == true && b == false) 
			{
					b = true; 
			}
			
			else if (leftStick.getRawButton(2) == true && b == true) 
			{
					b = false; 
			}
				
			if (b == true) {
				shooterFire.set(-.4); 
				//System.out.println("Pickup mode enabled.");
			}
			else if (b == false && a == false)
			{
				shooterFire.set(0); 
				//System.out.println("Pickup mode disabled.");
			}
*/			
			
				 	
			/*	while(true)
				{
					error = desiredRpm - encoder.getRate();
					
					*/
					
					
				/*	while(error>0)
					{
						//measure error, wait x seconds, measure error, (error 1 - error 2)/x*.00005 = Kp
						error1 = desiredRpm - encoder.getRate();
						try {
							Thread.sleep(10);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						error2 = desiredRpm - encoder.getRate();
						Kp = (error1 - error2)/.001*.00005;
						
								break;
					}
					currentPWM += (error*Kp);
					shooterFire.set(currentPWM);
					*/
					
					//System.out.println(error);
				}
		
		 //	if (leftStick.getRawButton(5))
		//	{
				//shooterFire.set(0);
		//	}
			
        }
        
    }

